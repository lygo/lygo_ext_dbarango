module bitbucket.org/lygo/lygo_ext_dbarango

go 1.16

require (
	bitbucket.org/lygo/lygo_commons v0.1.120
	github.com/arangodb/go-driver v0.0.0-20210825071748-9f1169c6a7dc
	github.com/google/uuid v1.3.0 // indirect
)
