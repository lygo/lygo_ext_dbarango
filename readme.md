# LyGo DB Arango
## ArangoDB wrapper

## How to Use

To use just call:

`go get -u bitbucket.org/lygo/lygo_ext_dbarango`

## Dependencies ##

`go get -u bitbucket.org/lygo/lygo_commons`

This module depend on [go-driver](https://github.com/arangodb/go-driver) 

`go get github.com/arangodb/go-driver`

`go get github.com/arangodb/go-driver/http`

### Versioning ###

Sources are versioned using git tags:

```
git tag v0.1.1
git push origin v0.1.1
```
